const form = document.querySelector('#task-form'); // select form
const taskList = document.querySelector('.collection'); //ul
const taskInput = document.querySelector('#task'); // select input box from form
const clearBtn = document.querySelector('.clear-tasks'); // btn clear
const filter = document.querySelector('#filter');


loadEventListeners();


function loadEventListeners(){

    document.addEventListener('DOMContentLoaded', getTasks);

    form.addEventListener('submit', addTask);

    taskList.addEventListener('click', removeTask);

    clearBtn.addEventListener('click', clearTasks);

    filter.addEventListener('keyup', filterTasks);
}


function addTask(e){
    if(taskInput.value === ""){
        console.log('alert');
        alert('add Task');
    } else {
        console.log('creat');
        const li = document.createElement('li');
        li.setAttribute('class','colection-item');
        li.textContent = taskInput.value;
        const link = document.createElement('a');
        link.setAttribute('class','delete-item secondary-content');
        link.innerHTML = '<i class="fa fa-remove"></i>';
        li.appendChild(link);

        taskList.appendChild(li);


        storeTaskInLocalStorage(taskInput.value);


        taskInput.value = '';

        e.preventDefault();
    }


}

function removeTask(e){
    
    if(e.target.parentElement.classList.contains('delete-item')){
        console.log(e.target.parentElement.parentElement);
        let parent = e.target.parentElement.parentElement;
        if(confirm('Are You Sure?')){
            parent.remove();

            removeTaskFromLocalStorage(parent);
    }
    }
   
}

function removeTaskFromLocalStorage(taskItem) {
    let tasks;
    if (localStorage.getItem('tasks') === null) {
        tasks = [];
    } else {
        tasks = JSON.parse(localStorage.getItem('tasks'));
    }

    tasks.forEach(function (task, index) {
        if (taskItem.textContent === task) {
            tasks.splice(index, 1);
        }
    });

    localStorage.setItem('tasks', JSON.stringify(tasks));
}

function clearTasks() {
    console.log('clear');
    taskList.innerHTML = '';

    clearTasksFromLocalStorage();

}


function clearTasksFromLocalStorage() {
    localStorage.clear();
  }

  
function filterTasks(e) {
    const text = e.target.value.toLowerCase();
    document.querySelectorAll('.colection-item').forEach(function(task){
        const item = task.firstChild.textContent;

        if(item.toLowerCase().indexOf(text) != -1) {
            task.style.display = 'block';
        } else {
            task.style.display = 'none';
        }
    });
}

function storeTaskInLocalStorage(task){
    let tasks;
    if(localStorage.getItem('tasks') === null){
        tasks = [];
    } else {
        tasks = JSON.parse(localStorage.getItem('tasks'));
    }
    tasks.push(task);

    localStorage.setItem('tasks', JSON.stringify(tasks));
}

function getTasks() {
    let tasks;

    if(localStorage.getItem('tasks') === null){
        tasks = [];
    } else {
        tasks = JSON.parse(localStorage.getItem('tasks'));
    }
    tasks.forEach(function (task) {
        // Create li element
        const li = document.createElement('li');
        // Add class
        li.className = 'collection-item';
        // Create text node and append to li
        li.appendChild(document.createTextNode(task));
        // Create new link element
        const link = document.createElement('a');
        // Add class
        link.className = 'delete-item secondary-content';
        // Add icon html
        link.innerHTML = '<i class="fa fa-remove"></i>';
        // Append the link to li
        li.appendChild(link);

        // Append li to ul
        taskList.appendChild(li);
    });
}